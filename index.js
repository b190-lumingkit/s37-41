const express = require("express");
const mongoose = require("mongoose");
// cors - control the app's Cross-Origin-Resource-Sharing settings
const cors = require("cors");

// import routes
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");


const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://jaycee24:brtMms9x3zndixW@wdc028-course-booking.8oezu.mongodb.net/b190-course-booking?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`We're connected to the database.`));

// allows all resources to access our backend application
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// User routes
app.use("/users", userRoutes);
// Course routes
app.use("/courses", courseRoutes);

/* 
    Heroku Deployment
        Procfile - needed file in heroku to determine the command that will be used when starting the server (web: node app)
*/



// process.env.PORT handles the environment of the hosting websites should app be hosted in a website such as herouke
app.listen(process.env.PORT || 4000, () => 
{ 
    console.log(`API now online at port ${process.env.PORT || 4000}`);
});