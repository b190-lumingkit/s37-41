const jwt = require("jsonwebtoken");
// used in algorithm for encrypting our data which makes it difficult to decode information without the defined secret keyword
const secret = "ChamberSentinel";

module.exports.createAccessToken = (user) => 
{
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    return jwt.sign(data, secret, {}); 
}

// Token Verification
module.exports.verify = (req, res, next) => 
{
    // the token is retrieved from the request header
    // POSTMAN - Authorization - Bearer Token
    let token = req.headers.authorization

    if (typeof token !== "undefined")
    {
        console.log(token)
        // the token sent is a type of "Bearer" token which when received, contains the "Bearer" as a prefix
        token = token.slice(7, token.length);
        // validates the token decrypting the token using the secret code
        return jwt.verify(token, secret, (err, data) =>
        {
            if (err) 
            {
                return res.send({auth: "failed"});    
            } 
            else 
            {
                next()
            }
        })
    }
    else 
    {
        res.send({auth: "failed"});
    }
}

// Token Decryption
module.exports.decode = (token) => 
{
    if (typeof token !== "undefined") 
    {
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (err, data) =>
        {
            if (err) {
                return null;
            } else {
                // .payload property contains the information provided in the "createAccessToken" method defined above (values for id, email, isAdmin)
                return jwt.decode(token, {complete: true}).payload;
            }
        });
    } 
    else
    {
        // if the token does not exist
        return null;
    }
}