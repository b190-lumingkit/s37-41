const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseController");

// Route for creating/adding a course
router.post("/", auth.verify, (req, res) =>
{
    const userData = auth.decode(req.headers.authorization);
    courseController.addCourse({isAdmin: userData.isAdmin}, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
});

// Route for getting all courses
router.get("/all", (req, res) =>
{
    courseController.getAllCourses()
    .then(result => res.send(result))
})

// Route for getting all active courses
router.get("/", (req, res) =>
{
    courseController.getActiveCourses()
    .then(result => res.send(result))
})

// Route for getting a specific course
router.get("/:courseId", (req, res) =>
{
    courseController.getCourse(req.params.courseId)
    .then(result => res.send(result))
})

// Route for updating a specific course
router.put("/:courseId", auth.verify, (req, res) =>
{
    const userData = auth.decode(req.headers.authorization);
    courseController.updateCourse(req.params.courseId, req.body)
    .then(result => res.send(result))
    .catch(error => res.send(error));
})

// Route for archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) =>
{
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;
    if (isAdmin) 
    {
        courseController.archiveCourse(req.params.courseId)
        .then(result => res.send(result))
        .catch(error => res.send(error));
    } 
    else 
    {
        res.send(`You have no authorization`);
    }  
})


module.exports = router;