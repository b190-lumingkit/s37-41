const express = require("express");
const router = express.Router();
const auth = require("../auth");

// Require controller modules
const userController = require("../controllers/userController");

// Route for checking if the user's email already exists in the database
// since we will pass a "body" from the request object, post method will be used as HTTP method even if the goal is to just check the database if there is an existing email
router.post("/checkEmail", (req, res) => 
{
    userController.checkEmailExists(req.body)
    .then(resultFromController => res.send(resultFromController));
})
// Route for user registration
router.post("/register", (req, res) =>
{
    userController.registerUser(req.body)
    .then(resultFromController => res.send(resultFromController));
})
// Route for user login
router.post("/login", (req, res) =>
{
    userController.loginUser(req.body)
    .then(resultFromController => res.send(resultFromController))
})

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) =>
{
    const userData = auth.decode(req.headers.authorization);
    userController.getProfile({userId: userData.id})
    .then(userDetails => res.send(userDetails))
});

// Route for enrolling a user
router.post("/enroll", auth.verify, (req, res) =>
{
    const isUser = auth.decode(req.headers.authorization);
    if (isUser !== null) 
    {
        const data = {
            userId: isUser.id,
            courseId: req.body.courseId
        }
        userController.enrollUser(data)
        .then(result => res.send(result))
    } 
    else 
    {
        res.send(`You need to login to enroll.`);
    }
})

module.exports = router;