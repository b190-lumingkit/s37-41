const Course = require("../models/Course");


module.exports.addCourse = (userData, courseData) => 
{
    // check if user is admin
    if (userData.isAdmin) 
    {
        let newCourse = new Course({
            name: courseData.name,
            description: courseData.description,
            price: courseData.price
        });
        
        return newCourse.save().then((course, error) =>
        {
            if (error) 
            {
                console.log(error)
                return false;
            } 
            else 
            {
                return course;
            }
        })
    } 
    else 
    {
        return Promise.reject('You are not authorized to access this page.');
    }
}

module.exports.getAllCourses = () =>
{
    return Course.find().then((courses, error) =>
    {
        if (error) 
        {
            console.log(error);
            return false;
        } 
        else 
        {
            return courses;
        }
    })
}

module.exports.getActiveCourses = () =>
{
    return Course.find({isActive : true}).then((activeCourses, error) =>
    {
        if (error) 
        {
            console.log(error);
            return false;
        } 
        else 
        {
            return activeCourses;
        }
    })
}

module.exports.getCourse = (courseId) =>
{
    return Course.findById(courseId).then((course, error) =>
    {
        if (error) 
        {
            console.log(error);
            return false;
        }
        else if (course === null)
        {
            return false;
        }
        else 
        {
            return course;
        }
    })
}

module.exports.updateCourse = (courseId, reqBody) =>
{
    return Course.findByIdAndUpdate(courseId, reqBody).then((course, error) =>
    {
        if (error) 
        {
            console.log(error);
            return false;
        }
        else 
        {
            return true;
        }
    })
}

// S40 ACTIVITY
module.exports.archiveCourse = (courseId) =>
{
    return Course.findByIdAndUpdate(courseId, { isActive: false }).then((course, error) =>
    {
        if (error) 
        {
            console.log(error);
            return false;
        }
        else 
        {
            return `Course archived`;
        }
    })
}