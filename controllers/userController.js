// Require bcrypt for password hash
const bcrypt = require("bcrypt")
// for user authentication
const auth = require("../auth")


// Require model modules
const User = require("../models/User");
const Course = require("../models/Course");

// check if email already exists
module.exports.checkEmailExists = (reqBody) => 
{
    return User.find({ email: reqBody.email }).then((result, error) => 
    {
        if (error) 
        {
            console.log(error);
            return false;
        } 
        else if (result.length > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    })
}

module.exports.registerUser = (reqBody) => 
{
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        // hashSync - bcrypt's method for encrypting the password of the user once they have successfully registered in our database
        /* 
            first parameter - the value to which the encryption will be done; in this case
            password  coming from the request body
            second parameter ("10") - dictates how many "salt" rounds are to be given to encrypt the value
        */
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user, error) => 
    {
        {
            if (error) 
            {
                return false;
            } else 
            {
                return true;
            }
        }
    }) 
}

module.exports.loginUser = (reqBody) =>
{
    return User.findOne({ email: reqBody.email }).then((result, error) => 
    {
        if (error) 
        {
            console.log(error);
            return false;
        } 
        // if user email does not exist
        else if (result === null)
        {
            return false;
        }
        else
        // if user email exists
        {
            // .compareSync decodes the encrypted password from the database and compares it to the password received
            // it's a good practice that if the value returned by a method/function is boolean, the variable name should be answerable by yes/no
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            if (isPasswordCorrect) 
            {
                return { access: auth.createAccessToken(result) }
            } 
            else 
            {
                return false;
            }
        }
    })
}

module.exports.getProfile = (userData) =>
{
    console.log(userData);
    return User.findById(userData.userId).then((result, error) =>
    {
        if (error) 
        {
            console.log(error);
            return false;
        } 
        else if (result === null)
        {
            return false;
        }
        else 
        {
            result.password = "";
            return result;
        }
    });
}

// enroll user
module.exports.enrollUser = async (data) =>
{
    let isCourseDuplicate = false;
    let isUserUpdated = await User.findById(data.userId).then(user =>
    {
        // check if user is enrolling to a duplicate course
        let isUserEnrolled = user.enrollments.find(e => e.courseId === data.courseId)
        if (isUserEnrolled === undefined)
        {
            user.enrollments.push({courseId: data.courseId});

            return user.save().then((user, error) =>
            {
                if (error) 
                {
                    console.log(error)
                    return false;
                } 
                else 
                {
                    return true;
                }
            })
        } 
        else 
        {
            isCourseDuplicate = true;
            return false;
        }
    })

    let isCourseUpdated = await Course.findById(data.courseId).then(course =>
    {
        if (isCourseDuplicate) 
        {
            return false;
        } 
        else 
        {
            course.enrollees.push({userId: data.userId});

            return course.save().then((course, error)=>
            {
                if (error) 
                {
                    console.log(error)
                    return false;
                } 
                else 
                {
                    return true;
                }
            })
        }
    })

    if (isUserUpdated && isCourseUpdated)
    {
        return true;
    }
    else
    {
        return false
    }
}